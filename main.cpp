#include <algorithm>
#include <cmath>
#include <deque>
#include <ios>
#include <iostream>
#include <string>
#include <random>
#include <utility>
#include <vector>
#include <iomanip>

size_t hash(int i)
{
    return i;
}

// Expensive to calculate stats
struct CalculatedStats
{
    size_t tableSize;
    size_t nItems;
    size_t largestBucket;
    size_t maxConsecutiveSplits;
    float averageBucketSize;
    float load;
    std::vector<size_t> bucketSizes;
};


template <typename K, typename V>
class LvHash 
{
public:
    LvHash(size_t size, float targetLoad=1)
        : tableSize{size}, targetLoad{targetLoad}
    {
        table.resize(tableSize);
    }

    void put(const K & k, const V & v)
    {
        ++_nItems;
        std::clog << "put " << k << ", " << v << "\n" 
            << "\tnItems = " << _nItems
            << "\ttable.size() = " << table.size() 
            << "\ttableSize = " << tableSize << "\n"
            << "\tsplit: " << split << "\n";

        const auto h = hash(k);
        const auto a = address(h);
        std::clog << "a = " << a << " for " << v << "\n";
        Bucket & b = table[a];
        b.push_back( {k, v} );

        // Re-hash policy
#if 1
        size_t splits = 0;
        do {
            const float l = load();
            std::clog << "load = " << l << "\n";
            if (l < targetLoad) 
                break;
            std::clog << "Exceeded targetLoad\n";
            rehash();
            ++splits;
        } while (true);
        _maxConsecutiveSplits = std::max(_maxConsecutiveSplits, splits);
#else
        // There was a bucket collision
        if (b.size() > 1) 
            rehash();
#endif
    }

    float load() const
    {
        //return static_cast<float>(_nItems) / table.size();
        return static_cast<float>(_nItems) / (tableSize + split);
    }

    void rehash()
    {
        std::clog << "rehashing at split " << split << "\n";
        table.resize( table.size() + 1 );
        ++split;
        const auto srcIdx = split - 1;
        Bucket & b = table[srcIdx];
        Bucket old{b};
        b.clear();
        _nItems -= old.size();
        for (const auto & x : old) {
            const auto a = address(hash(x.first));
            Bucket & b = table[a];
            b.push_back(x);
            ++_nItems;
        }
        if (split == tableSize) {
            std::clog << "resetting split\n";
            split = 0;
            tableSize = table.size();
        }
    }

    size_t address(size_t hash)
    {
        size_t a;
        const auto h0 = hash % tableSize;
        const auto h1 = hash % (tableSize << 1);
        if (h0 < split) {
            a = h1;
        } else {
            a = h0; 
        }
        return a;
    }


    const V & get(const K & k)
    {
        const auto h = hash(k);
        const auto index = address(h);
        const Bucket & b = table[index];
        for (const auto & x : b) {
            if (x.first == k)
                return x.second;
        }
        throw std::runtime_error("Not found");
    }

    bool exists(const K & k)
    {
        const auto h = hash(k);
        const auto index = address(h);
        const Bucket & b = table[index];
        for (const auto & x : b) {
            if (x.first == k)
                return true;
        }
        return false;
    }


    CalculatedStats calculateStats() const 
    {
        CalculatedStats s {};
        size_t largest = 0;
        float avg = 0.0;

        for (const auto & b : table) {
            avg = avg + b.size();
            largest = std::max( largest, b.size() );
            s.bucketSizes.push_back( b.size() );
        }
        if (table.size())
            avg /= table.size();
        s.averageBucketSize = avg;
        s.largestBucket = largest;
        s.nItems = _nItems;
        s.tableSize = table.size();
        s.load = load();
        s.maxConsecutiveSplits = _maxConsecutiveSplits;
        return s;
    }

    size_t tableSize;
    float targetLoad;

    size_t _maxConsecutiveSplits{0};
    size_t _nItems {0};
    size_t split {0};

    using Bucket = std::deque< std::pair<K,V> >;
    std::vector<Bucket> table;
};


int main(void)
{
    std::cout.imbue(std::locale(""));
    std::random_device dev;
    std::mt19937_64 generator(dev());
    std::uniform_int_distribution<int64_t> distribution;
    int n = distribution(generator); 
#if 1
    std::clog.setstate(std::ios::failbit);
#endif

    constexpr auto initialSize = 2;
    constexpr auto loadFactor = 0.8; // 20.0f;
    LvHash<int64_t, int64_t> lvHash{initialSize, loadFactor};
    const int canary = 42;
    lvHash.put(canary, canary);
    auto i = 1000000;
    while (i) {
        --i;

#if 1 // random
        int n = distribution(generator); 
#elif 0
        int n = i; 
#elif 0
        int n = 50;
#endif
#if 1 // Unique insertions
        if (lvHash.exists(n))
            continue;
#endif
        lvHash.put(n, n);
        lvHash.get(canary);
#if 0
        { 
            // Random lookups
            int n = distribution(generator); 
            lvHash.exists(n);
        }
#endif
    }
    CalculatedStats s = lvHash.calculateStats();
    std::cout << std::fixed 
        << "averageBucketSize: " << s.averageBucketSize << "\n"
        << "largestBucket: " << s.largestBucket << "\n"
        << "nItems: " << s.nItems << "\n"
        << "table.size(): " << s.tableSize << "\n"
        << "load: " << s.load << "\n"
        << "maxConsecutiveSplits: " << s.maxConsecutiveSplits << "\n"
        << "\n";
    std::clog << "bucket sizes:\n";
    for (const auto & x : s.bucketSizes) {
        std::clog << "\t" << x;
    }
    std::clog << "\n";
}
